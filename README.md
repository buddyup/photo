# buddyup-photo

各個人の写真をつなげて、集合写真にできる

## 設定（すごくわかっている人だけがやってください）

firebase storageから取得したデータを加工するために、gcloudでcorsの設定をする必要がある。
- https://developer.mozilla.org/ja/docs/Web/HTML/CORS_enabled_image
- https://qiita.com/niusounds/items/383a780d46ee8551e98c

この環境ではすでに実施済みです。
```
gcloud auth login
gcloud config set project buddyupcommunities
gsutil cors set firebase.storage.cors.json gs://buddyupcommunities.appspot.com

```
```
gsutil cors get gs://buddyupcommunities.appspot.com 
```