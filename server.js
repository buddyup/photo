var express = require("express"),
  app = express(),
  port = process.env.PORT || 5001,
  bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

//var routes = require("./api/routes/wordRoutes"); // Routeのインポート
//routes(app); //appにRouteを設定する。

app.listen(port); // appを特定のportでlistenさせる。

console.log("words RESTful API server started on: " + port);