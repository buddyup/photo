async function loadImages(urls) {
  return Promise.all(
    urls.map(url => {
      const image = new Image()
      image.src = url
      return new Promise(resolve => {
        image.onload = (() => resolve(image))
      })
    })
  )
}

function arrange(images, canvas) {
  const ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let x = 0, y = 0;
  for(const image of images) {
    if (x >= canvas.width) {
      x = 0;
      y += 100;
    }
    if (y >= canvas.height) {
      break;
    }
    ctx.drawImage(image, x, y, 100, 100);
    x += 100;
  }            
}
