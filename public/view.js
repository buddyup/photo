function getMyPhotoDB(community, id) {
  return firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .collection("members")
    .doc(id)
    .get()
    .then((doc) => doc.data());
}
