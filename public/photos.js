function watchPhotos(community, onUpdate, limit = 600) {
  let ref = firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .collection("members");

  if (limit) ref = ref.limit(limit);

  ref.onSnapshot((snapshot) => {
    onUpdate(snapshot.docs.map((doc) => doc.data()));
  });
}

function countPhotos(photos) {
  return photos.reduce((acc, doc) => {
    if (acc[doc.organization]) {
      acc[doc.organization]++;
    } else {
      acc[doc.organization] = 1;
    }
    return acc;
  }, {});
}

async function getCommunityData(community) {
  if (!community) return {};
  return (
    (await firebase
      .firestore()
      .collection("photos")
      .doc(community)
      .get()
      .then((doc) => doc.data())) || {}
  );
}

function watchCommunityData(community, onUpdate) {
  if (!community) return;
  let first = true;
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection("photos")
      .doc(community)
      .onSnapshot((doc) => {
        onUpdate(doc.data());
        if (first) {
          first = false;
          resolve();
        }
      });
  });
}

async function setOrganizations(community, orgs) {
  if (!community) return;
  return firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .set({ organizations: orgs }, { merge: true });
}

async function setRoles(community, roles) {
  if (!community) return;
  return firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .set({ roles: roles }, { merge: true });
}

function setSelect(select, orgs) {
  for (let org of orgs) {
    // optionタグを作成する
    var option = document.createElement("option");
    // optionタグのテキストを4に設定する
    option.text = org;
    // optionタグのvalueを4に設定する
    option.value = org;
    // selectタグの子要素にoptionタグを追加する
    select.appendChild(option);
  }
}

/////////////////////////////////////////////////
// id
/////////////////////////////////////////////////

function getBrowserId(community) {
  if (!community) {
    return getRandomId(16);
  }
  try {
    let id = localStorage.getItem(`buddyup-photo_${community}`);
    if (id) return id;
    else {
      id = getRandomId(16);
      localStorage.setItem(`buddyup-photo_${community}`, id);
      return id;
    }
  } catch (e) {
    return getRandomId(16);
  }
}

function getRandomId(length) {
  let l = 20;
  if (!Number.isFinite(length) || length < 1) {
    l = 20;
  } else {
    l = length;
  }

  const n = Math.floor(l);
  if (crypto) {
    // cryptoがあればそちらを利用
    const N = Math.floor((n / 4) * 3);
    return btoa(
      String.fromCharCode(...crypto.getRandomValues(new Uint8Array(N)))
    )
      .substring(0, n)
      .replace(/\//g, "0")
      .replace(/\+/g, "A")
      .replace(/=/g, "a");
  } else {
    // cryptoがなければMath.random()を利用
    const S = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return Array.from(Array(n))
      .map(() => S[Math.floor(Math.random() * S.length)])
      .join("");
  }
}
