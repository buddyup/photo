//////////////////////////////////////////////////////////
// group photos
//////////////////////////////////////////////////////////

///////////////////////////////////////////////
// save
//

/**
 * イメージをfirebase上のstrageに保存し、保存先urlを返す
 * @param community - communityid
 * @param org - organization
 * @param id - 集合写真のid
 * @param photoData - アプロード対象イメージ　dataURL
 * @param metaData
 * metaData = {
 *   url: url,
 *   organization: organization,
 *   title: title,
 *   createdBy: browserid
 *   uid: buddyup!からの場合にuserのid
 * };
 */

async function saveGroupPhoto(community, org = "all", id, photoData, metaData) {
  metaData.url = await uploadGroupPhotoStorage(photoData, community, org, id);
  await addGroupPhotoDB(community, org, id, metaData);
}

// @returns {String} - イメージファイル保存先url
async function uploadGroupPhotoStorage(
  photoData,
  community = "others",
  org = "all",
  id
) {
  // https://firebase.google.com/docs/storage/web/upload-files?hl=ja
  function isUploadable(photoURL) {
    if (!photoURL) return false;
    if (photoURL.indexOf("data:image/png;base64") === -1) return false;
    return true;
  }
  if (!isUploadable(photoData)) {
    return "";
  }
  const org2 = org.replace(
    /[\/\#\$\&\'\"\\\,\[\]\{\}\|\~\=\*\`\+\;\:\\(\)`]/g,
    ""
  );
  const filePath = `images/groupPhotos/${community}/${org2}/${id}.png`;

  const uploadTask = await firebase
    .storage()
    .ref()
    .child(filePath)
    .putString(photoData, "data_url");
  const url = await uploadTask.ref.getDownloadURL();
  return url;
}

async function addGroupPhotoDB(community, org, id, data) {
  if (!data) return;
  data.organization = org;
  data.updated = firebase.firestore.FieldValue.serverTimestamp();
  // timestamp = doc.data().timestamp.toDate(); でDate型に戻すらしい

  if (id) {
    firebase
      .firestore()
      .collection("photos")
      .doc(community)
      .collection("groupPhotos")
      .doc(id)
      .set(data);
  }
}

///////////////////////////////////////////////
// watch
//

function watchGroupPhotos(community, onUpdate) {
  firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .collection("groupPhotos")
    .onSnapshot((snapshot) => {
      onUpdate(
        snapshot.docs.map((doc) => Object.assign({ id: doc.id }, doc.data()))
      );
    });
}

///////////////////////////////////////////////
// remove
//

async function removeGroupPhoto(community, groupPhoto) {
  return Promise.all([
    removeGroupPhotoStorage(groupPhoto.url),
    removeGroupPhotoDB(community, groupPhoto.id),
  ]);
}

async function removeGroupPhotoDB(community, id) {
  if (!id) return;
  return firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .collection("groupPhotos")
    .doc(id)
    .delete();
}

async function removeGroupPhotoStorage(filePath) {
  if (!filePath) return;
  const path = filePath
    .replace(/^(http|https):\/\/.*\//, "/")
    .replace(/\?.*/, "")
    .replace(/%2F/g, "/");

  return firebase
    .storage()
    .ref()
    .child(path)
    .delete();
}

///////////////////////////
// loadImage, arrange
///////////////////////////

async function loadImages(urls) {
  return Promise.all(
    urls.map((url) => {
      const image = new Image();
      image.crossOrigin = "Anonymous";
      image.src = url;
      return new Promise((resolve) => {
        image.onload = () => resolve(image);
      });
    })
  );
}

function arrange(images, canvas) {
  const ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let x = 0,
    y = 0;
  for (const image of images) {
    if (x >= canvas.width) {
      x = 0;
      y += 100;
    }
    if (y >= canvas.height) {
      break;
    }
    ctx.drawImage(image, x, y, 100, 100);
    x += 100;
  }
}

//////////////////////////////////////////////////////
//
// UI
//
//////////////////////////////////////////////////////

function setGroupPhotoView(
  community,
  communityData,
  groupPhoto,
  parentElement
) {
  const div = document.createElement("div");

  const aTag = document.createElement("a");
  aTag.href = groupPhoto.url;
  aTag.target = "_blank";
  aTag.appendChild(document.createTextNode(groupPhoto.title));

  const removeButton = document.createElement("button");
  removeButton.appendChild(document.createTextNode("削除する"));
  removeButton.addEventListener("click", () => {
    removeGroupPhoto(community, groupPhoto);
    parentElement.removeChild(div);
  });

  const defaultButton = document.createElement("button");
  defaultButton.appendChild(document.createTextNode("デフォルトにする"));
  defaultButton.addEventListener("click", () => {
    pickDefaultGroupPhoto(community, communityData, groupPhoto, parentElement);
    parentElement.removeChild(div);
  });

  div.appendChild(aTag);
  div.appendChild(removeButton);
  div.appendChild(defaultButton);
  parentElement.appendChild(div);
}

/////////////////////////////////////////////////
// make it default

async function pickDefaultGroupPhoto(community, communityData, groupPhoto, parentElement) {
  const org = groupPhoto.organization;
  if((communityData.groupPhotos || {})[org]) {
    setGroupPhotoView(community, communityData, communityData.groupPhotos[org], parentElement);
  }

  return firebase
    .firestore()
    .collection("photos")
    .doc(community)
    .set(
      {
        groupPhotos: {
          [groupPhoto.organization]: groupPhoto,
        },
      },
      { merge: true }
    );
}

function makeDefaultView(communityData, org, defaultView) {
  defaultView.innerText = "";
  const groupPhoto = (communityData.groupPhotos || {})[org];
  if (!groupPhoto) return;
  const div = document.createElement("div");

  const aTag = document.createElement("a");
  aTag.href = groupPhoto.url;
  aTag.target = "_blank";
  aTag.appendChild(document.createTextNode(groupPhoto.title));

  div.appendChild(aTag);
  defaultView.appendChild(div);
}
