async function upload(
  communityid,
  browserid,
  photoData1,
  photoData2,
  metaData,
  userType = "general"
) {
  metaData.medium = await uploadImage(photoData1, communityid, browserid + "_m");
  metaData.small = await uploadImage(photoData2, communityid, browserid + "_s");
  await addIndexToDB(communityid, metaData, browserid, userType);
}

/**
 * イメージをfirebase上のstrageに保存し、保存先urlを返す
 * @param {} photoData - アプロード対象イメージ
 * @param community - communityid
 * @param id - id
 * @returns {String} - イメージファイル保存先url
 */

async function uploadImage(photoData, community = "others", id) {
  // https://firebase.google.com/docs/storage/web/upload-files?hl=ja
  if (!isUploadable(photoData)) {
    return "";
  }
  const filePath = `images/photos/${community}/${id}.png`;

  const uploadTask = await firebase
    .storage()
    .ref()
    .child(filePath)
    .putString(photoData, "data_url");
  const url = await uploadTask.ref.getDownloadURL();
  return url;
}


/**
 * 指定されたイメージのアップロード可否を判定
 * @param {} photoURL - アプロード対象イメージ
 */
function isUploadable(photoURL) {
  if (!photoURL) return false;
  if (photoURL.indexOf("data:image/png;base64") === -1) return false;
  return true;
}

/**
 * DBにエントリを追加
 *
 */
async function addIndexToDB(communityid, data, id) {
  if (!data || !data.medium || !data.small || !data.organization || !data.name)
    return;
  // const data = {
  //   url: url,
  //   organization: organization,
  //   name: name,
  //   role: role,
  //   uid: uid, // buddyup!のuidがある場合のみ
  // };
  data.updated = firebase.firestore.FieldValue.serverTimestamp();
  // timestamp = doc.data().timestamp.toDate(); でDate型に戻すらしい

  if (id) {
    firebase
      .firestore()
      .collection("photos")
      .doc(communityid)
      .collection("members")
      .doc(id)
      .set(data);
  }
}


////////////////////////////////////////////

function onFileChange(e) {
  let file = e.target.files[0];
  if (file && file.type.match(/^image\/(png|jpeg)$/)) {
    handleImage(file, 360, (dataURL) => {
      this.set("names.photoURL", dataURL);
    });
  }
}

/**
 * イメージファイルの角度などを調整
 */
function handleImage(file, max = 480, func) {
  let reader = new FileReader();

  // 写真のEXIF情報のOrientationを取得
  let orientation = 1;
  EXIF.getData(file, () => {
    orientation = EXIF.getTag(file, "Orientation");
    if (!orientation) {
      return;
    }
  });

  reader.onload = (readerEvent) => {
    let image = new Image();
    image.onload = () => {
      // Resize the image
      let canvas = document.createElement("canvas");
      let width = image.width;
      let height = image.height;
      // 圧縮のためmaxに合わせてリサイズ
      if (width > height) {
        if (width > max) {
          height *= max / width;
          width = max;
        }
      } else {
        if (height > max) {
          width *= max / height;
          height = max;
        }
      }

      canvas.width = width;
      canvas.height = height;

      if (
        orientation == 5 ||
        orientation == 6 ||
        orientation == 7 ||
        orientation == 8
      ) {
        canvas.width = height;
        canvas.height = width;
      }

      let ctx = canvas.getContext("2d");
      ctx.save();

      // 写真のEXIF情報のOrientationに基づいて回転させる
      let rotate = 0;
      let wmove = 0;
      let hmove = 0;
      switch (orientation) {
        case 3:
          rotate = 180;
          wmove = -width;
          hmove = -height;
          break;
        case 6:
          rotate = 90;
          wmove = 0;
          hmove = -height;
          break;
        case 8:
          rotate = -90;
          wmove = -width;
          hmove = 0;
          break;
      }
      ctx.rotate((rotate * Math.PI) / 180);
      ctx.translate(wmove, hmove);
      ctx.drawImage(image, 0, 0, width, height);
      ctx.restore();
      func(canvas.toDataURL("image/png"));
    };
    image.src = readerEvent.target.result;
  };
  if (file) {
    reader.readAsDataURL(file);
  }
}
